package org.vanis.jee.examples.hornetqdlq.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

@MessageDriven(mappedName = "HappyQueue", activationConfig = {
        @ActivationConfigProperty(propertyName="destination", propertyValue="java:/jms/queue/DLQ"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class DLQMDB implements MessageListener {

    private static final Logger log = LoggerFactory.getLogger(DLQMDB.class);

    @Resource(mappedName = "java:jboss/datasources/ba-datasource")
    private DataSource dataSource;

    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        String messageText = null;
        try {
            messageText = textMessage.getText();
        } catch (JMSException e) {
            log.error("Error reading message: ", e);
        }

        log.info("Message from DQL recieved: " + messageText);
        Integer taskId = Integer.parseInt(messageText);
        try {
            logErrorCause(taskId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        dumpProperties(message);
    }

    private void logErrorCause(int taskId) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement pstmt = connection.prepareStatement("select time, message from log where task_id = ?");
        pstmt.setInt(1, taskId);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next()){
            log.info(String.format("This task %s has following associated cause", "" + taskId));
            log.info(rs.getString(2));
        }
    }


    private void dumpProperties(Message message) {
        try {
            Enumeration<String> e = message.getPropertyNames();
            while (e.hasMoreElements()) {
                String key = e.nextElement();
                String value = message.getStringProperty(key);
                log.info(String.format("Property %s has value %s", key, value));
            }
        } catch (JMSException e) {
            log.error("Error while dumping props: ", e);
        }

    }

}
