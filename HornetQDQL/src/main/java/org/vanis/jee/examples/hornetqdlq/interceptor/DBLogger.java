package org.vanis.jee.examples.hornetqdlq.interceptor;

import javax.inject.Qualifier;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * Created by vanisp on 13.5.2015.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, PARAMETER, TYPE})
public @interface DBLogger {
}