package org.vanis.jee.examples.hornetqdlq.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vanis.jee.examples.hornetqdlq.entities.Task;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Named("taskDAO")
public class TaskDAO {

    private static final Logger log = LoggerFactory.getLogger(TaskDAO.class);

    @PersistenceContext(unitName="mypu")
    EntityManager em;

    public TaskDAO() {
    }

    @SuppressWarnings("unchecked")
    public Collection<Task> getAllTasks() {
        List<Task> tasks = new ArrayList<Task>();
        Query query = em.createQuery("SELECT t FROM Task t");
        return query.getResultList();
    }

    public Task getTask(int id){
        return em.find(Task.class, id);
    }

    public void updateTask(Task t){
        em.merge(t);
        em.flush();
    }

    public void saveTask(Task t) {
        em.persist(t);
        em.flush();
    }
}
