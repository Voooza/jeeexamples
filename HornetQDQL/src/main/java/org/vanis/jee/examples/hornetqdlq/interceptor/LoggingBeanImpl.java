package org.vanis.jee.examples.hornetqdlq.interceptor;

import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import javax.transaction.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vanisp on 13.5.2015.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@DBLogger
public class LoggingBeanImpl implements LoggingBean{

    private org.slf4j.Logger log = LoggerFactory.getLogger(LoggingBeanImpl.class);

    @Resource(mappedName = "java:jboss/datasources/ba-datasource")
    private DataSource dataSource;

    @Resource
    private UserTransaction tx;

    public LoggingBeanImpl() {
    }

    public void logException(int taskId, String message){
        try {
            doLog(taskId, message);
        } catch (SQLException | SystemException | HeuristicRollbackException | NotSupportedException | RollbackException | HeuristicMixedException e) {
            log.error("Failed to log message. ", e);
        }
    }

    private void doLog(int taskId, String message) throws SQLException, SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        tx.begin();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Connection connection = dataSource.getConnection();
        PreparedStatement pstmt = connection.prepareStatement("insert into log(task_id, time, message) values(?, ?, ?)");
        pstmt.setInt(1,taskId);
        pstmt.setString(2, sdf.format(new Date()));
        pstmt.setString(3, message);
        pstmt.executeUpdate();
        tx.commit();
    }
}
