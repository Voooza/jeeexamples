package org.vanis.jee.examples.hornetqdlq.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vanis.jee.examples.hornetqdlq.interceptor.Catchable;
import org.vanis.jee.examples.hornetqdlq.dao.TaskDAO;
import org.vanis.jee.examples.hornetqdlq.entities.Task;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

@MessageDriven(mappedName = "HappyQueue",
               activationConfig = { @ActivationConfigProperty(propertyName = "destination",
                                                              propertyValue = "java:/jms/queue/HappyQueue"), @ActivationConfigProperty(propertyName = "acknowledgeMode",
                                                                                                                                       propertyValue = "Auto-acknowledge"),
                       @ActivationConfigProperty(propertyName = "destinationType",
                                                 propertyValue = "javax.jms.Queue") })
public class HappyMDB implements MessageListener {

    private static Logger log = LoggerFactory.getLogger(HappyMDB.class);

    @Inject
    private TaskDAO taskDAO;

    @Override
    public void onMessage(Message message) {
        try {
            doOnMessage(message);
        } catch (Exception e) {
            if (e instanceof RuntimeException){
                // All exceptions that occur here should be runtime exceptions
                // Interceptor wraps everything to RuntimeException.
                throw (RuntimeException) e;
            }else{
                // but to be extra safe:
                throw new RuntimeException(e);
            }
        }

    }

    @Catchable  // See this?
    private void doOnMessage(Message message) throws Exception {
        TextMessage textMessage = (TextMessage) message;
        String messageText = "DEFAULT";
        try {
            messageText = textMessage.getText();
        } catch (JMSException e) {
            log.error("Error reading message: ", e);
        }
        log.info("Message recieved: " + messageText);

        Integer taskId = 0;
        try {
            taskId = Integer.parseInt(messageText);
        } catch(NumberFormatException e){
            log.error(String.format("String %s cannot be parsed to int!", messageText));
        }


        createTask(taskId);

        updateTask(taskId);

        exceptionThrower();
    }

    private void createTask(int taskId) {
        Task task = new Task();
        task.setTaskId(taskId);
        task.setTaskType("TASKTYPE");
        task.setNote("MYNOTE: " + getDateString());
        task.setDependentTaskId(1);
        task.setParentTaskId(1);
        taskDAO.saveTask(task);
        sleep(3);
    }

    private void updateTask(int taskId) {
        Task task2 = taskDAO.getTask(taskId);
        task2.setTaskType("UPDATED - task type");
        taskDAO.updateTask(task2);
        sleep(3);
    }

    private void sleep(int duration) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(duration));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private String getDateString(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    private void dumpProperties(Message message) {
        try {
            @SuppressWarnings("unchecked")
            Enumeration<String> e = message.getPropertyNames();
            while (e.hasMoreElements()) {
                String key = e.nextElement();
                String value = message.getStringProperty(key);
                log.info(String.format("Property %s has value %s", key, value));
            }
        } catch (JMSException e) {
            log.error("Error while dumping props: ", e);
        }

    }

    private void exceptionThrower() throws Exception {
        throw new Exception("My exception");
    }
}
