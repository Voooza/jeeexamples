package org.vanis.jee.examples.hornetqdlq.interceptor;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.jms.Message;
import javax.jms.TextMessage;

@Catchable
@Interceptor
public class ExceptionInterceptor {

    private Logger log = LoggerFactory.getLogger(ExceptionInterceptor.class);

    @Inject @DBLogger
    LoggingBean loggingBean;

    @AroundInvoke
    private Object intercept(InvocationContext ic) throws Exception {
        Object ret = null;
        try {
            log.info(" <<< pre-proceed");
            ret = ic.proceed();
        }catch(Exception e){
            Message m = (Message) ic.getParameters()[0];
            TextMessage tm = (TextMessage) m;
            String cause = ExceptionUtils.getFullStackTrace(e);
            String message = tm.getText();
            Integer taskId = Integer.parseInt(message);
            loggingBean.logException(taskId, cause);
            log.info(String.format("Message body: %s", tm.getText()));
            throw new RuntimeException(e);
        } finally {
            log.info(" >>> post-proceed");
        }
        return ret;
    }


}
