package org.vanis.jee.examples.hornetqdlq.interceptor;

/**
 * Created by vanisp on 13.5.2015.
 */
public interface LoggingBean {

    void logException(int taskId, String message);
}
