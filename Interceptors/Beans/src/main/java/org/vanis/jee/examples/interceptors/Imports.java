package org.vanis.jee.examples.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

public class Imports {

    @Produces
    public Logger produceLogger(InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger(injectionPoint.getMember()
                .getDeclaringClass()
                .getName());
    }

    private Imports() {
        // Disable instantiation of this class
    }
}
