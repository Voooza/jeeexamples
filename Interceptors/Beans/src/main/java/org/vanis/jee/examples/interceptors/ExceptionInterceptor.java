package org.vanis.jee.examples.interceptors;

import org.slf4j.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.annotation.Priority;

@Catchable
@Interceptor
@Priority(Interceptor.Priority.APPLICATION+10)
public class ExceptionInterceptor {

    @Inject
    private Logger log;

    @AroundInvoke
    private Object intercept(InvocationContext ic) throws Exception {
        try {
            log.info("pre-proceed");
            return ic.proceed();
        }catch(Exception e){
            Message m = (Message) ic.getParameters()[0];
            TextMessage tm = (TextMessage) m;
            log.info(String.format("Message body: %s", tm.getText()));
            throw e;
        } finally {
            log.info("post-proceed");
        }
    }
}
