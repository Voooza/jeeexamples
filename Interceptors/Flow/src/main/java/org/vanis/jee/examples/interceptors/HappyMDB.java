package org.vanis.jee.examples.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(mappedName = "HappyQueue",
               activationConfig = { @ActivationConfigProperty(propertyName = "destination",
                                                              propertyValue = "java:/jms/queue/HappyQueue"), @ActivationConfigProperty(propertyName = "acknowledgeMode",
                                                                                                                                       propertyValue = "Auto-acknowledge"),
                       @ActivationConfigProperty(propertyName = "destinationType",
                                                 propertyValue = "javax.jms.Queue") })
public class HappyMDB implements MessageListener {

    private static Logger log = LoggerFactory.getLogger(HappyMDB.class);

    @Catchable
    public void onMessage(Message message) {
        throw new RuntimeException("YOU SUCK!");
    }

    private void exceptionThrower() {
        throw new RuntimeException("My runtime exception");
    }
}
