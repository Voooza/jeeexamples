package org.vanis.jee.examples.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.jms.*;

@Singleton
public class TimedMessanger {

    private static final Logger log = LoggerFactory.getLogger(TimedMessanger.class);

    @Resource(mappedName = "java:/jms/queue/HappyQueue")
    private Queue queue;

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory cf;


    private Connection connection;

    @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
    public void fire() {
        log.info("Timer fired");
        try {
            connection = cf.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer publisher = null;

            publisher = session.createProducer(queue);

            connection.start();

            log.info("Sending message");
            TextMessage message = session.createTextMessage("Hello message");
            publisher.send(message);


        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {


            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}